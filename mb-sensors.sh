#!/bin/bash
# script that reacds from MBP sensors files and displays temperature readings
#

function LogMessage () {
    echo "$1"
}

lsTempSensors=()
lsTempCores=()

devPlatform=/sys/devices/platform
applesmc=${devPlatform}/applesmc.768
coretemp=${devPlatform}/coretemp.0

# mobo sensors
Temp1InputFn=${applesmc}/temp1_input
Temp2InputFn=${applesmc}/temp2_input
Temp3InputFn=${applesmc}/temp3_input
Temp4InputFn=${applesmc}/temp4_input
Temp5InputFn=${applesmc}/temp5_input
Temp6InputFn=${applesmc}/temp6_input
Temp7InputFn=${applesmc}/temp7_input
Temp8InputFn=${applesmc}/temp8_input
Temp9InputFn=${applesmc}/temp9_input
Temp10InputFn=${applesmc}/temp10_input
Temp11InputFn=${applesmc}/temp11_input
Temp12InputFn=${applesmc}/temp12_input
Temp13InputFn=${applesmc}/temp13_input
Temp14InputFn=${applesmc}/temp14_input

lsSensors=( ${Temp1InputFn} ${Temp2InputFn} ${Temp3InputFn} ${Temp4InputFn} ${Temp5InputFn} ${Temp6InputFn} ${Temp7InputFn} ${Temp8InputFn} ${Temp9InputFn} ${Temp10InputFn} ${Temp11InputFn} ${Temp12InputFn} ${Temp13InputFn} ${Temp14InputFn} )

# CPU cores
CoreTemp1Fn=$(find "${coretemp}/hwmon" -type f -name temp2_input)
CoreTemp2Fn=$(find "${coretemp}/hwmon" -type f -name temp3_input)

if [[ ! -f ${CoreTemp1Fn} ]]; then
    LogMessage "Fatal Error: CoreTemp1Fn ${CoreTemp1Fn} file does not exist!"
else
    LogMessage "CoreTemp1Fn found: ${CoreTemp1Fn}"
fi

if [[ ! -f ${CoreTemp2Fn} ]]; then
    LogMessage "Fatal Error: CoreTemp2Fn ${CoreTemp2Fn} file does not exist!"
else
    LogMessage "CoreTemp2Fn found: ${CoreTemp2Fn}"
fi

lsCores=( ${CoreTemp1Fn} ${CoreTemp2Fn} )

Fan1Fn=${applesmc}/fan1_min
Fan2Fn=${applesmc}/fan2_min

function ReadFromFile () {
    local afile="$1"
    local line=$( cat ${afile} | head -1 )
    # trim the string:
    line=$(echo ${line} | xargs)
    local result=$(echo ${line[0]})
    echo ${result}
}

function ListArray () {
    local -n alist=$1
    local tcounter=1
    for i in "${alist[@]}";do
        LogMessage "  ${tcounter}:  ${i/\.*}"
        tcounter=$(echo "${tcounter} + 1" | bc -l)
    done
}

function GetAvgTemp () {
    local -n alist=$1
    local result=40
    local tsum=0
    local counter=0
    for num in "${alist[@]}";do
        if (( $(echo "${num} > 0.01" | bc -l) ));then
            tsum=$(echo "${tsum} + ${num}" | bc -l)
            counter=$(echo "${counter} + 1" | bc -l)
        fi
    done
    if (( $(echo "${counter} > 0.01" | bc -l) ));then
        result=$(echo "${tsum} / ${counter}" | bc -l)
    fi
    echo ${result/\.*}
}

function GetTempReadings () {
    lsTempSensors=()
    lsTempCores=()
    local counter=0
    for sfn in "${lsSensors[@]}";do
        local val=$(ReadFromFile ${sfn})
        val=$(echo "${val} / 1000" | bc -l)
        lsTempSensors[${counter}]=${val}
        counter=$(echo "${counter} + 1" | bc -l)
    done
    counter=0
    for cfn in "${lsCores[@]}";do
        local val=$(ReadFromFile ${cfn})
        val=$(echo "${val} / 1000" | bc -l)
        lsTempCores[${counter}]=${val}
        counter=$(echo "${counter} + 1" | bc -l)
    done
}

GetTempReadings

AverageTempSensors=$(GetAvgTemp lsTempSensors)
AverageTempCores=$(GetAvgTemp lsTempCores)

AverageTemp=$(echo "(${AverageTempSensors} + ${AverageTempCores}) / 2.0" | bc -l)
AverageTemp=${AverageTemp/\.*}

function ListAll () {
    LogMessage "+++ Mobo Sensors +++"
    ListArray lsTempSensors
    LogMessage "---"

    LogMessage "+++ CPU Sensors +++"
    ListArray lsTempCores
    LogMessage "---"


    LogMessage "+++ Average Temps +++"
    LogMessage "Average Sensors : ${AverageTempSensors/\.*}"
    LogMessage "Average Cores   : ${AverageTempCores/\.*}"
    LogMessage "Average Temp    : ${AverageTemp/\.*}"
    LogMessage "---"

    local rpm1=$(ReadFromFile ${Fan1Fn})
    local rpm2=$(ReadFromFile ${Fan2Fn})
    LogMessage "Fan 1: ${rpm1} RPM"
    LogMessage "Fan 2: ${rpm2} RPM"

}

ListAll
