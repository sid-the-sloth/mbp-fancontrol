#!/bin/bash
#
# this script can be used by systemd service + timer
#
# in case you wish to use cron, the crontab looks like this:
# * * * * * /usr/local/sbin/mb-fancontrol.sh
# * * * * * (sleep 20 ; /usr/local/sbin/mb-fancontrol.sh )
# * * * * * (sleep 40 ; /usr/local/sbin/mb-fancontrol.sh )
#

# Make sure only root can run this script
if [ "$(id -u)" != "0" ]; then
    echo "Error: This script must be run as root." 1>&2
    exit 1
fi

# set to 1 for debugging
DebugLogging=0
Moq=0

# Tweak these numbers to adjust the sensitivity of the fans
MaxTemp=65
MinTemp=35

# These numbers are tweakable as well but note that 6200 is the highest setting for the fan.
MaxFanSpeed=6200
MinFanSpeed=2200

function LogMessage () {
    logger -t $(basename $0) "$1"
}

function MathMin () {
    local result=$2
    if (( $(echo "$1 < $2" | bc -l) ));then
        result=$1
    fi
    echo ${result}
}

function MathMax () {
    local result=$2
    if (( $(echo "$1 > $2" | bc -l) ));then
        result=$1
    fi
    echo ${result}
}

# params: min, max, value
function MathInRange () {
    local result=$3
    local themin=$(MathMin $1 $2)
    local themax=$(MathMax $1 $2)

    result=$(MathMin $3 ${themax})
    result=$(MathMax ${result} ${themin})

    echo ${result}
}

function ListArray () {
    local -n alist=$1
    local tcounter=1
    for i in "${alist[@]}";do
        LogMessage "  ${tcounter}:  ${i/\.*}"
        tcounter=$(echo "${tcounter} + 1" | bc -l)
    done
}

# Function to calculate the fan speed based on temperature
function CalcFanSpeed () {
    local paramTemp=$1
    #local fspeed=$(echo "((${paramTemp} - ${MinTemp}) * (${MaxFanSpeed} - ${MinFanSpeed}) / (${MaxTemp} - ${MinTemp})) + ${MinFanSpeed}" | bc -l)
    local fspeed=$(echo "((${paramTemp} - ${MinTemp}) * (${MaxFanSpeed} - ${MinFanSpeed}) / (${MaxTemp} - ${MinTemp})) + ${MinFanSpeed}" | bc -l)

    #if (( $(echo "${paramTemp} - ${PrevTemp} > 4.0" | bc -l) ));then
        ## if there is a spike
        #fspeed=$(echo "${fspeed} * 1.25" | bc -l)
    #elif (( $(echo "${paramTemp} < ${PrevTemp}" | bc -l) ));then
        ## cooling
        #if (( $(echo "${paramTemp} > ${MinTemp}" | bc -l) ));then
            #fspeed=$(echo "${fspeed} * 1.15" | bc -l)
        #fi
    #elif (( $(echo "${paramTemp} > 43.01" | bc -l) ));then
        ## if the temp is 45 or so:
        #fspeed=$(echo "${fspeed} * 1.15" | bc -l)
    #fi

    # sanity check for min and max speeds
    fspeed=$(MathInRange ${MinFanSpeed} ${MaxFanSpeed} ${fspeed})
    echo ${fspeed/\.*}
}

function GetAvgTemp () {
    local -n alist=$1
    local result=40
    local tsum=0
    local counter=0
    for num in "${alist[@]}";do
        if (( $(echo "${num} > 0.01" | bc -l) ));then
            tsum=$(echo "${tsum} + ${num}" | bc -l)
            counter=$(echo "${counter} + 1" | bc -l)
        fi
    done
    if (( $(echo "${counter} > 0.01" | bc -l) ));then
        result=$(echo "${tsum} / ${counter}" | bc -l)
    fi
    echo ${result/\.*}
}

# Returns only the first line from the file
function ReadFromFile () {
    local afile="$1"
    local line=$( cat ${afile} | head -1 )
    # trim the string:
    line=$(echo ${line} | xargs)
    local result=$(echo ${line[0]})
    echo ${result}
}

function WriteToFile () {
    local afile="$1"
    local avalue=$2
    echo ${avalue} > ${afile}
}

function GetCrtTemp () {
    if [[ -f "${CrtTempFn}" ]]; then
        CrtTemp=$(ReadFromFile ${CrtTempFn})
    else
        touch "${CrtTempFn}"
        CrtTemp=${MinTemp}
        SetCrtTemp ${CrtTemp}
    fi
    CrtTemp=${CrtTemp/\.*}
    echo ${CrtTemp}
}

function SetCrtTemp () {
    CrtTemp=$1
    CrtTemp=${CrtTemp/\.*}
    WriteToFile ${CrtTempFn} ${CrtTemp}
}

function SetFanSpeed () {
    local FanSpeed=$1
    FanSpeed=$(MathMin ${FanSpeed} ${MaxFanSpeed})
    FanSpeed=$(MathMax ${FanSpeed} ${MinFanSpeed})

    if [[ "${Moq}" == "1" ]];then
        local blah=0
    else
        WriteToFile ${Fan1Fn} ${FanSpeed}
        WriteToFile ${Fan2Fn} ${FanSpeed}
    fi
    if [[ "${DebugLogging}" == "1" ]];then
        LogMessage "Fan speed set to: ${FanSpeed}"
    fi
}

devPlatform=/sys/devices/platform
applesmc=${devPlatform}/applesmc.768
coretemp=${devPlatform}/coretemp.0

if [[ ! -d ${applesmc} ]]; then
    LogMessage "Fatal Error: ${applesmc} does not exist!"
    exit 1
fi
if [[ ! -d ${coretemp} ]]; then
    LogMessage "Fatal Error: ${coretemp} does not exist!"
    exit 1
fi

CrtTempFn=/opt/temp_current

Fan1Fn=${applesmc}/fan1_min
Fan2Fn=${applesmc}/fan2_min

# mobo sensors
Temp1InputFn=${applesmc}/temp1_input
Temp2InputFn=${applesmc}/temp2_input
Temp3InputFn=${applesmc}/temp3_input
Temp4InputFn=${applesmc}/temp4_input
Temp5InputFn=${applesmc}/temp5_input
Temp6InputFn=${applesmc}/temp6_input
Temp7InputFn=${applesmc}/temp7_input
Temp8InputFn=${applesmc}/temp8_input
Temp9InputFn=${applesmc}/temp9_input
Temp10InputFn=${applesmc}/temp10_input
Temp11InputFn=${applesmc}/temp11_input
Temp12InputFn=${applesmc}/temp12_input
Temp13InputFn=${applesmc}/temp13_input
Temp14InputFn=${applesmc}/temp14_input

lsSensors=( ${Temp1InputFn} ${Temp2InputFn} ${Temp3InputFn} ${Temp4InputFn} ${Temp5InputFn} ${Temp6InputFn} ${Temp7InputFn} ${Temp8InputFn} ${Temp9InputFn} ${Temp10InputFn} ${Temp11InputFn} ${Temp12InputFn} ${Temp13InputFn} ${Temp14InputFn} )

# for Ubuntu Lucid:
# CoreTemp1Fn=/sys/devices/platform/coretemp.0/temp1_input
# CoreTemp2Fn=/sys/devices/platform/coretemp.1/temp1_input

# for CentOS:
# CoreTemp1Fn=/sys/devices/platform/coretemp.0/temp1_input
# CoreTemp2Fn=/sys/devices/platform/coretemp.0/temp2_input

# for Ubuntu Precise 12.04:
# CoreTemp1Fn=/sys/devices/platform/coretemp.0/temp2_input
# CoreTemp2Fn=/sys/devices/platform/coretemp.0/temp3_input

# for Ubuntu Trusty 18.04:
# CoreTemp1Fn=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp2_input
# CoreTemp2Fn=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp3_input

# for Ubuntu Focal 20.04:
# CoreTemp1Fn=/sys/devices/platform/coretemp.0/hwmon/hwmon4/temp2_input
# CoreTemp2Fn=/sys/devices/platform/coretemp.0/hwmon/hwmon4/temp3_input

# for Debian bullseye 11:
# CoreTemp1Fn=/sys/devices/platform/coretemp.0/hwmon/hwmon6/temp2_input
# CoreTemp2Fn=/sys/devices/platform/coretemp.0/hwmon/hwmon6/temp3_input

CoreTemp1Fn=$(find "${coretemp}/hwmon" -type f -name temp2_input)
CoreTemp2Fn=$(find "${coretemp}/hwmon" -type f -name temp3_input)

if [[ ! -f ${CoreTemp1Fn} ]]; then
    LogMessage "Fatal Error: CoreTemp1Fn ${CoreTemp1Fn} file does not exist!"
else
    if [[ "${DebugLogging}" == "1" ]];then
        LogMessage "CoreTemp1Fn found: ${CoreTemp1Fn}"
    fi
fi

if [[ ! -f ${CoreTemp2Fn} ]]; then
    LogMessage "Fatal Error: CoreTemp2Fn ${CoreTemp2Fn} file does not exist!"
else
    if [[ "${DebugLogging}" == "1" ]];then
        LogMessage "CoreTemp2Fn found: ${CoreTemp2Fn}"
    fi
fi

lsCores=( ${CoreTemp1Fn} ${CoreTemp2Fn} )

function GetTempReadings () {
    lsTempSensors=()
    lsTempCores=()
    local counter=0
    for sfn in "${lsSensors[@]}";do
        local val=$(ReadFromFile ${sfn})
        if [[ "${val}" == "" ]];then
            val=80000
        fi
        val=$(echo "${val} / 1000" | bc -l)
        lsTempSensors[${counter}]=${val}
        counter=$(echo "${counter} + 1" | bc -l)
    done

    counter=0
    for cfn in "${lsCores[@]}";do
        local val=$(ReadFromFile ${cfn})
        if [[ "${val}" == "" ]];then
            val=80000
        fi
        val=$(echo "${val} / 1000" | bc -l)
        lsTempCores[${counter}]=${val}
        counter=$(echo "${counter} + 1" | bc -l)
    done
}

if [[ "${DebugLogging}" == "1" ]];then
    LogMessage "===== Starting ====="
fi

lsTempSensors=()
lsTempCores=()

GetTempReadings

if [[ "${DebugLogging}" == "1" ]];then
    LogMessage "Sensors Temperatures:"
    ListArray lsTempSensors
    LogMessage "Cores Temperatures:"
    ListArray lsTempCores
fi

# record previous temp
PrevTemp=$(GetCrtTemp)

AverageTempSensors=$(GetAvgTemp lsTempSensors)
AverageTempCores=$(GetAvgTemp lsTempCores)

AverageTemp=$(echo "(${AverageTempSensors} + ${AverageTempCores}) / 2.0" | bc -l)
AverageTemp=${AverageTemp/\.*}

# record current temp
#SetCrtTemp ${AverageTemp}
SetCrtTemp ${AverageTempCores}

if [[ "${DebugLogging}" == "1" ]];then
    LogMessage "Average Sensors Temperature: ${AverageTempSensors/\.*}"
    LogMessage "Average Cores Temperature: ${AverageTempCores/\.*}"
    LogMessage "Average Temperature: ${AverageTemp/\.*}"
fi

FanSpeed=$(CalcFanSpeed ${CrtTemp})
SetFanSpeed ${FanSpeed}

#crtFanSpeed=$(ReadFromFile ${Fan1Fn})
LogMessage "Sensors: ${AverageTempSensors/\.*}, Cores: ${AverageTempCores/\.*}, Fan Speed: ${FanSpeed}"
