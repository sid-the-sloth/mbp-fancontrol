# mbp-fancontrol
Bash script which can be run in Linux cron in order to control the cooling fans' speed on a MacBook Pro cca 2007

Instructions:<br>
1. Mark the script as executable<br>
2. Copy the script to `/usr/local/sbin/mb-fancontrol.sh`<br>
3. Edit root's crontab and add the following lines:
>\* * * * * /usr/local/sbin/mb-fancontrol.sh<br>
>\* * * * * (sleep 30 ; /usr/local/sbin/mb-fancontrol.sh )
